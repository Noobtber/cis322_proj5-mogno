# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database.

Maintained by Justin Becker (jbecker4@uoregon.edu)

## What is in this repository

This is everything you need for Docker-compose to run a brevet calculator and store the data in MongoDB.

## Functionality

To run, navigate to the dockermongo folder, run docker-compose build and docker-compose up. It will be hosted locally at port 5000.

If you click display when there are no points, it will give you an error page.

If you click Submit with no data, it will not submit anything.

## Tasks

You'll turn in your credentials.ini using which we will get the following:

* The working application.

* A README.md file that includes not only identifying information (your name) but but also a revised, clear specification 
  of the brevet controle time calculation rules.

* Dockerfile

* Test cases for the two buttons. No need to run nose.

* docker-compose.yml

## Grading Rubric

* If your code works as expected: 100 points. This includes:
	* AJAX in the frontend. That is, open and close times are automatically populated, 
	* Frontend to backend interaction (with correct requests/responses), 
	* README is updated with your name and email.

* If the AJAX logic is not working, 10 points will be docked off. 

* If the README is not clear or missing, up to 15 points will be docked off. 

* If the two test cases fail, up to 15 points will be docked off. 

* If the logic to enter into or retrieve from the database is wrong, 30 points will be docked off.

* If none of the functionalities work, 30 points will be given assuming 
    * The credentials.ini is submitted with the correct URL of your repo, and
    * Dockerfile is present 
    * Docker-compose.yml works/builds without any errors 

* If the Docker-compose.yml doesn't build or is missing, 10 points will be docked off.

* If credentials.ini is missing, 0 will be assigned.
